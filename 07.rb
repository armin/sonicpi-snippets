cutoff = 33
live_loop :foo do
  
  cutoff = cutoff + rrand_i(-5,10)
  use_synth :saw
  
  with_fx :ping_pong, mix: 0.7, feedback: 0.8, pre_amp: 1, phase: 0.33333 do
    live_loop :echoes do
      play chord([:b1, :b2, :e1, :e2, :b3, :e3].choose, :minor).choose, cutoff: rrand(40, 100), amp: 0.3, attack: 0, release: rrand(1, 2), cutoff_max: 110
      sleep [0.25, 0.25, 0.5, 0.5, 1].choose
    end
  end
  
  sample :glitch_bass_g, amp: 0.5
  
  play chord([:b1, :b2, :e1, :e2, :b3, :e3].choose, :minor).choose, cutoff: rrand(40, 100), amp: 0.9, attack: 0, release: rrand(1, 2), cutoff_max: 110
  
  play :b1, release: 0, cutoff: cutoff, decay: 0.1, atack: 0.1, sustain: 0.01; sleep 0.25
  play :b1, release: 0, cutoff: cutoff, decay: 0.1, attack: 0.1, sustain: 0.01; sleep 0.25
  play :b1, release: 0, cutoff: cutoff, decay: 0.1, sustain: 0.01; sleep 0.25
  play :b1, release: 0, cutoff: cutoff, decay: 0.1, sustain: 0.01; sleep 0.25
  
  play :b1, release: 0, cutoff: cutoff, decay: 0.1, atack: 0.1, sustain: 0.01; sleep 0.25
  play :b1, release: 0, cutoff: cutoff, decay: 0.1, attack: 0.1, sustain: 0.01; sleep 0.25
  play :b1, release: 0, cutoff: cutoff, decay: 0.1, sustain: 0.01; sleep 0.25
  play :b1, release: 0, cutoff: cutoff, decay: 0.1, sustain: 0.01; sleep 0.25
  
  play :d1, release: 0, cutoff: cutoff, decay: 0.1, atack: 0.1, sustain: 0.01; sleep 0.25
  play :d1, release: 0, cutoff: cutoff, decay: 0.1, attack: 0.1, sustain: 0.01; sleep 0.25
  play :d1, release: 0, cutoff: cutoff, decay: 0.1, sustain: 0.01; sleep 0.25
  play :d1, release: 0, cutoff: cutoff, decay: 0.1, sustain: 0.01; sleep 0.25
  
  play :e1, release: 0, cutoff: cutoff, decay: 0.1, atack: 0.1, sustain: 0.01; sleep 0.25
  play :e1, release: 0, cutoff: cutoff, decay: 0.1, attack: 0.1, sustain: 0.01; sleep 0.25
  play :e1, release: 0, cutoff: cutoff, decay: 0.1, sustain: 0.01; sleep 0.25
  play :e1, release: 0, cutoff: cutoff, decay: 0.1, sustain: 0.01; sleep 0.25
  
  if cutoff > 40 then ( cutoff = (cutoff -10) )
  end
  if cutoff < -20 then ( cutoff = (cutoff +10) )
  end
  
end


