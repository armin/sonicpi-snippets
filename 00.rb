live_loop :foo do
  use_synth :rodeo
  with_fx :ring_mod, freq: 50, mod_amp: 0.2 do
    with_fx :flanger, phase: [1,2,4,8,16,32].choose, mix: rrand(0.2,0.5) do
      with_fx :echo, mix: 0.7, decay: 0.9, phase: 0.3, max_phase: 3 do
        with_fx :pitch_shift, pitch_dis: 0.8 do
          play_pattern_timed [40, 40, 52, 40], [0.5, 0.25, 0.75, 0.5], slide: 2, amp: 0.2
        end
      end
    end
  end
  sleep 8
end
live_loop :foo2 do
  use_synth :sine
  play_pattern_timed [40, 40, 55, 54, 40, 40, 52, 50 ], [0.5, 0.25, 0.75, 0.5, 0.5, 0.25, 0.75, 0.5], sustain: 0.1, dec: 0.1, amp: 0.7
end
live_loop :foo3 do
  use_synth :tri
  play_pattern_timed [40, 40, 64, 62, 40, 40, 55, 54 ], [0.5, 0.25, 0.75, 0.5, 0.5, 0.25, 0.75, 0.5], sustain: 0.01, amp: 0.2, decay: 0.1
end
with_fx :autotuner do
  with_fx :vowel, voice: 3, vowel_sound: 4 do
    with_fx :ping_pong, mix: 0.6, feedback: rrand(0.4,0.6), pre_amp: 1.2, phase: rrand(0.1,0.7), max_phase: 3 do
      with_fx :tremolo, wave: 1, depth: 0.6 do
        with_fx :ring_mod, freq: 0.01, mod_amp: 1, mix: 1 do
          with_fx :ixi_techno, phase_offset: rrand(0.2,0.8), cutoff_min: 20, cutoff_max: 90, res: 0.7 do
            live_loop :foo4 do
              use_synth :square
              play_pattern_timed [28, 0, 28, 0, 28, 0, 28, 40, 28, 0, 28, 0, 0, 0, 0, 0 ], [0.5, 0.25, 0.75, 0.5, 0.5, 0.25, 0.75, 0.5], sustain: 0.01, amp: 0.2, decay: 0.1
            end
          end
        end
      end
    end
  end
end



