# track bpm
use_bpm 122

# track enable/disable
bd_enabled = 1
robot_enabled = 0
guit_enabled = 0
glitch_enabled = 0
piano_enabled = 0
fx_enabled = 0

# loop
live_loop :arp do
  with_fx :ping_pong, mix: 0.5, feedback: 0.9, pre_amp: 5, phase: rrand(0.5,4) do
    use_synth :blade
    play chord(:a, :minor, num_octaves: 1), release: 0.8, amp: 0.2, rate: 0.125, cutoff: rrand(80,120)
  end
  if guit_enabled == 1
    sample :guit_e_fifths, rate: 0.5
  end
  if glitch_enabled == 1
    with_fx :ping_pong, mix: 0.5, feedback: 0.9, pre_amp: 5, phase: rrand(0.1,5) do
      sample :glitch_robot1, rate: 0.5, amp: 0.2
    end
  end
  if robot_enabled == 1
    sample "/Users/armin/samples/40-TelegramSamplepacksNew/samplepacksrussia_20210528/CYMATICS/FX/Cymatics - Empire FX 1 - Impact Delay.wav", amp: 0.5, attack: 0.2, pan: rrand(-0.5,0.5)
  end
  sample :ambi_choir, rate: 0.2
  if bd_enabled == 1
    sample :bd_haus, rate: 1
  end
  sleep 1
  sample :ambi_choir, rate: 0.2
  if bd_enabled == 1
    sample :sn_dub
    sample :bd_haus, rate: 1
  end
  sleep 1
  sample :ambi_choir, rate: 0.2
  if bd_enabled == 1
    sample :bd_haus, rate: 1
  end
  
  sleep 1
  sample :ambi_choir, rate: 0.2
  if bd_enabled == 1
    sample :sn_dub
    sample :bd_haus, rate: 1
  end
  
  sleep 1
  sample :ambi_choir, rate: 0.2
  if bd_enabled == 1
    sample :bd_haus, rate: 1
  end
  
  sleep 1
  sample :ambi_choir, rate: 0.2
  if bd_enabled == 1
    sample :sn_dub
    sample :bd_haus, rate: 1
  end
  sleep 1
  sample :ambi_choir, rate: 0.25
  if bd_enabled == 1
    sample :bd_haus, rate: 1
  end
  sleep 1
  sample :ambi_choir, rate: 0.25
  if bd_enabled == 1
    sample :sn_dub
    sample :bd_haus, rate: 1
  end
  sleep 0.75
  sample :ambi_choir, rate: 0.5
  if bd_enabled == 1
    sample :bd_haus, rate: 1
  end
  sleep 0.25
  if piano_enabled == 1
    sample :ambi_piano, amp: 1.5, attack: 0.04, rate: 1.321, pan: rrand(-0.5,0.5)
  end
  sample :guit_em9, rate: -1, amp: 0.2, beat_stretch: 0.25
  sample :ambi_choir, rate: 0.2
  if bd_enabled == 1
    sample :bd_haus, rate: 1
  end
  sleep 1
  sample :ambi_choir, rate: 0.2
  if bd_enabled == 1
    sample :sn_dub
    sample :bd_haus, rate: 1
  end
  sleep 1
  sample :ambi_choir, rate: 0.2
  if bd_enabled == 1
    sample :bd_haus, rate: 1
  end
  sleep 1
  sample :ambi_choir, rate: 0.2
  if bd_enabled == 1
    sample :sn_dub
    sample :bd_haus, rate: 1
  end
  sleep 1
  sample :ambi_choir, rate: 0.2
  if bd_enabled == 1
    sample :bd_haus, rate: 1
  end
  sleep 1
  sample :ambi_choir, rate: 0.2
  if bd_enabled == 1
    sample :sn_dub
    sample :bd_haus, rate: 1
  end
  sleep 1
  sample :ambi_choir, rate: 0.25
  if bd_enabled == 1
    sample :bd_haus, rate: 1
  end
  sleep 1
  sample :ambi_choir, rate: 0.25
  if bd_enabled == 1
    sample :sn_dub
    sample :bd_haus, rate: 1
  end
  sleep 0.5
  sample :ambi_choir, rate: 0.5
  if bd_enabled == 1
    sample :bd_haus, rate: 1
  end
  sleep 0.5
  if fx_enabled == 1
    with_fx :ping_pong, mix: 0.5, feedback: 0.9, pre_amp: 5, phase: rrand(0.1,5) do
      sample :misc_cineboom, rate: rrand(0.7,1.3), amp: 0.2
    end
  end
end

