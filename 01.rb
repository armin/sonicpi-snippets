use_bpm 122

live_loop :foo do
  use_synth :tri
  with_fx :ping_pong, mix: 0.5, feedback: 0.7, pre_amp: 1.3, phase: 0.75 do
    with_fx :flanger, depth: 30, phase: Math::cos(Math::sin(vt/96.0).abs).abs, decay: 3, phase_offset: Math::sin(vt).abs do
      with_fx :rlpf, cutoff: 64 + (vt % 30).abs do
        play chord(:c3, :minor)
        sleep 0.75
        play chord(:c3, :minor)
        sleep 0.25
        play chord(:c3, :minor)
        puts vt % 60
        puts Math::sin(vt % 60)
      end
    end
  end
  sleep 3
end

live_loop :beep1 do
  with_fx :echo, phase: 1.5, decay: 26 do
    with_fx :bitcrusher, bits: 4 do
      sync "/live_loop/foo"
      play [52,55], decay: rrand(0.2,0.9)
      sleep 12
    end
  end
end

live_loop :beep2 do
  with_fx :echo, phase: 1.5, decay: 26 do
    with_fx :bitcrusher, bits: 6 do
      sync "/live_loop/foo"
      sleep 2
      use_synth :sine
      play [64,67,73], decay: rrand(0.2,0.9), amp: 0.4
      sleep 10
    end
  end
end

live_loop :beep3 do
  with_fx :echo, phase: 3, decay: 26 do
    with_fx :bitcrusher, bits: 5 do
      sync "/live_loop/foo"
      sleep 4
      use_synth :sine
      play [64,67,73], decay: rrand(0.2,0.9), amp: 0.4
      sleep 8
    end
  end
end



