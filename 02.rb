use_bpm 102

with_fx :ping_pong, feedback: 0.84, phase: 0.66666666 do
  live_loop :unique do
    sample "/Users/armin/Library/Application Support/Renoise/V3.3.2/Library/Samples/Stabs/ps_keys_1.flac", pitch: -8
    sleep 16
  end
end

live_loop :beat do
  sample "/Users/armin/Library/Application Support/Renoise/V3.3.2/Library/Samples/Breaks/SM101_brk_DMC_102bpm.flac"
  sleep 16
end

with_fx :lpf, cutoff: 64 do
  live_loop :melo do
    use_synth :tb303
    play 37, pitch: -12
    sleep 6.5
    play 40, pitch: -12
    sleep 0.666666666666
    play 42, pitch: -12
    sleep 0.833333333333
    play 37, pitch: -12
    sleep 8
  end
end




