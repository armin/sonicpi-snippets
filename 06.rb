use_bpm 126

with_fx :reverb, mix: 0.7 do
  live_loop :l1 do
    play scale(:Eb3, :major_pentatonic).choose, release: 0.1, amp: rrand(0.8,1.0), amp: 0.4
    sleep 0.666666666666
  end
end

with_fx :reverb, mix: 0.2 do
  live_loop :l2 do
    play scale(:Eb2, :major_pentatonic, num_octaves: 3).choose, release: 0.3, amp: rand
    a = scale(:Eb2, :major_pentatonic, num_octaves: 3).choose
    puts a
    sleep 0.33333333333333
  end
  sleep 0.33333333333333
end

live_loop :l3 do
  sync "/live_loop/l1"
  use_synth :square
  play [48,48,48,48,48,48,48,51,46,46,46,46,46,46,46,53].tick, release: 0.2, amp: rrand(0.8,1.0)
end

live_loop :l3 do
  sample "/Users/armin/Library/Application Support/Renoise/V3.3.2/Library/Multi-Samples/Bass/Digital1 Samples/BassDigital1_C-4.flac"
  sleep 8
end

live_loop :l4 do
  sample "/Users/armin/Library/Application Support/Renoise/V3.3.2/Library/Multi-Samples/Key/Rhodish Samples/Rhodish_C-8.flac", rate: 0.03125
  sleep 4
end











