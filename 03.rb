load_samples(sample_names :perc)
sleep 0.3


with_fx :reverb, mix: 0.7, room: 1 do
  with_fx :ping_pong, mix: 0.3, feedback: 0.7, pre_amp: 5, phase: rrand(0.1,5) do
    live_loop :foo do
      
      sp_name = ["/Users/armin/samples/40-TelegramSamplepacksNew/samplepacksrussia_20210528/CYMATICS/FX/Cymatics - Empire FX 1 - Impact Delay.wav", "/Users/armin/samples/40-TelegramSamplepacksNew/samplepackz_20210528/VMH2 FX Sounds/VMH2 FX Sounds 170.wav", "/Users/armin/samples/40-TelegramSamplepacksNew/samplepackz_20210528/VMH2 FX Sounds/VMH2 FX Sounds 152.wav", "/Users/armin/samples/40-TelegramSamplepacksNew/samplepackz_20210528/VMH2 FX Sounds/VMH2 FX Sounds 157.wav"]
      sp_time = rrand(0.6,5)
      sp_rate = 1
      s = sample sp_name, cutoff: rrand(40, 130), rate: sp_rate * choose([0.5, 0.6]), pan: rrand(-0.5, 0.5), pan_slide: sp_time
      control s, pan: rrand(-0.5, 0.5)
      sleep sp_time
      
      sp_name = ["/Users/armin/samples/40-TelegramSamplepacksNew/samplepacksrussia_20210528/CYMATICS/FX/Cymatics - Empire FX 4 - Drone Reese.wav", "/Users/armin/samples/07-DnB_Liquidism/LIQUIDISM - LIQ_SOUNDS_&_FX/LIQ_INST_HITS/LIQ_Dm_Ether_Synth_Chord.wav", "/Users/armin/samples/07-DnB_Liquidism/LIQUIDISM - LIQ_SOUNDS_&_FX/LIQ_INST_HITS/LIQ_Cm_Full_Sad_Keys.wav"].choose
      sp_time = rrand(0.6,5)
      sp_rate = rrand(0.3,2.3)
      s = sample sp_name, cutoff: rrand(40, 130), rate: sp_rate * rrand(0.3,3.4) + 0.1, pan: rrand(-0.5, 0.5), pan_slide: sp_time
      control s, pan: rrand(-0.5, 0.5)
      sleep sp_time
      
      sp_name = choose sample_names :ambi
      sp_time = rrand(0.6,5)
      sp_rate = rrand(0.7,1.3)
      s = sample sp_name, cutoff: rrand(40, 130), rate: sp_rate * rrand(0.3,2.3), pan: rrand(-0.5, 0.5), pan_slide: sp_time
      control s, pan: rrand(-0.5, 0.5)
      sleep sp_time
      
      sleep 0.4
      
    end
  end
end




