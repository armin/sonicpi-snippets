use_bpm 110

live_loop :addon do
  # sample "/Users/armin/Library/Application Support/Renoise/V3.3.2/Library/Multi-Samples/Comb/Additive1 Samples/ZAdditive2_F#3.wav"
  sample "/Users/armin/Library/Application\ Support/Renoise/V3.3.2/Library/Samples/Breaks/SM101_brk_Bangy\ Drummer_110bpm.flac"
  sleep 16
end

live_loop :stab do
  sample "/Users/armin/Library/Application Support/Renoise/V3.3.2/Library/Samples/Stabs/ech_stab_deephoused_C.flac"
  sleep 8
end

live_loop :stab2 do
  sleep 13
  sample "", pitch: -2
  sleep 3
end

live_loop :pad do
  sample "/Users/armin/Library/Application Support/Renoise/V3.3.2/Library/Samples/Stabs/ps_chord_replace_me_1.flac", pitch: -10
  sleep 8
end

live_loop :synth do
  use_synth :dpulse
  play [36,32].tick
  sleep 4
end

